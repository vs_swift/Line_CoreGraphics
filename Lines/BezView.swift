//
//  BezView.swift
//  Lines
//
//  Created by Private on 2/25/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit
import CoreGraphics

class BezView: UIView {
    private var shapeLayer = CAShapeLayer()
    override func draw(_ rect:CGRect) {
        drawLine()
    }
    private func drawLine() {
        let w = self.bounds.size.width
        let h = self.bounds.size.height
        
        let aPath = UIBezierPath()
        
        aPath.move(to: CGPoint(x:w*0.1, y: h*0.1))
        aPath.addLine(to: CGPoint(x:w*0.1, y: h*0.9))
        aPath.move(to: CGPoint(x:w*0.1, y: h*0.9))
        aPath.addLine(to: CGPoint(x:w*0.9, y: h*0.9))
        aPath.move(to: CGPoint(x:w*0.9, y: h*0.9))
        aPath.addLine(to: CGPoint(x:w*0.9, y: h*0.2))
        
        aPath.move(to: CGPoint(x:w*0.9, y: h*0.2))
        aPath.addLine(to: CGPoint(x:w*0.2, y: h*0.2))
        aPath.move(to: CGPoint(x:w*0.2, y: h*0.2))
        aPath.addLine(to: CGPoint(x:w*0.2, y: h*0.8))
        
        aPath.move(to: CGPoint(x:w*0.2, y: h*0.8))
        aPath.addLine(to: CGPoint(x:w*0.8, y: h*0.8))
        aPath.move(to: CGPoint(x:w*0.8, y: h*0.8))
        aPath.addLine(to: CGPoint(x:w*0.8, y: h*0.3))
        
        aPath.move(to: CGPoint(x:w*0.8, y: h*0.3))
        aPath.addLine(to: CGPoint(x:w*0.3, y: h*0.3))
        aPath.move(to: CGPoint(x:w*0.3, y: h*0.3))
        aPath.addLine(to: CGPoint(x:w*0.3, y: h*0.7))
        
        aPath.move(to: CGPoint(x:w*0.3, y: h*0.7))
        aPath.addLine(to: CGPoint(x:w*0.7, y: h*0.7))
        aPath.move(to: CGPoint(x:w*0.7, y: h*0.7))
        aPath.addLine(to: CGPoint(x:w*0.7, y: h*0.4))
        
        
        shapeLayer.removeFromSuperlayer()
        shapeLayer.fillColor = UIColor.green.cgColor
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 6
        shapeLayer.path = aPath.cgPath
        self.layer.addSublayer(shapeLayer)
    }
}

